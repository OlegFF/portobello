<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
if(isset($_POST['code']) && $_POST['code'] > 0)
{
    \Bitrix\Main\Loader::includeModule('iblock');
    $arrEvent = \Bitrix\Iblock\ElementTable::query()
        ->addSelect('*')
        ->addFilter('ID', $_POST['code'])
        ->exec()
        ->fetchAll();
    if($arrEvent) {
        echo CUtil::PhpToJSObject($arrEvent);
    }
}else{

}
?>
