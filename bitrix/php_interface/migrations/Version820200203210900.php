<?php

namespace Sprint\Migration;


class Version820200203210900 extends Version
{
    protected $description = "Секции инфоблока Мероприятия";

    protected $moduleVersion = "3.12.17";

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $helper = $this->getHelperManager();

        $iblockId = $helper->Iblock()->getIblockIdIfExists(
            'infoportal_news_s17',
            'news'
        );

        $helper->Iblock()->addSectionsFromTree(
            $iblockId,
            array (
  0 => 
  array (
    'NAME' => 'Культура',
    'CODE' => '1culture',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '114',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  1 => 
  array (
    'NAME' => 'Общество',
    'CODE' => '1society',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '111',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  2 => 
  array (
    'NAME' => 'Политика',
    'CODE' => '1politics',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '109',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  3 => 
  array (
    'NAME' => 'Происшествия',
    'CODE' => '1incidents',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '112',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  4 => 
  array (
    'NAME' => 'Спорт',
    'CODE' => '1sports',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '113',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  5 => 
  array (
    'NAME' => 'Экономика',
    'CODE' => '1economy',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '110',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  6 => 
  array (
    'NAME' => 'Недвижимость',
    'CODE' => '1real_estate',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '203',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
    'CHILDS' => 
    array (
      0 => 
      array (
        'NAME' => 'Покупка',
        'CODE' => '1estate_purchase',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '209',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      1 => 
      array (
        'NAME' => 'Продажа',
        'CODE' => '1estate_for_sale',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '210',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      2 => 
      array (
        'NAME' => 'Разное',
        'CODE' => '1estate_miscellaneous',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '213',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      3 => 
      array (
        'NAME' => 'Сдаю',
        'CODE' => '1i_rent',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '211',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      4 => 
      array (
        'NAME' => 'Сниму',
        'CODE' => '1rent',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '212',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
    ),
  ),
  7 => 
  array (
    'NAME' => 'Оргтехника',
    'CODE' => '1office_equipment',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '205',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
    'CHILDS' => 
    array (
      0 => 
      array (
        'NAME' => 'Покупка',
        'CODE' => '1purchase',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '206',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      1 => 
      array (
        'NAME' => 'Продажа',
        'CODE' => '1equipment_for_sale',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '207',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      2 => 
      array (
        'NAME' => 'Разное',
        'CODE' => '1miscellaneous',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '208',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
    ),
  ),
  8 => 
  array (
    'NAME' => 'Транспорт',
    'CODE' => '1transportation',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '204',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
    'CHILDS' => 
    array (
      0 => 
      array (
        'NAME' => 'ГСМ',
        'CODE' => '1gsm',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '216',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      1 => 
      array (
        'NAME' => 'Покупка',
        'CODE' => '1trans_purchase',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '214',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      2 => 
      array (
        'NAME' => 'Продажа',
        'CODE' => '1for_sale',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '215',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      3 => 
      array (
        'NAME' => 'Услуги',
        'CODE' => '1services',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '217',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
    ),
  ),
  9 => 
  array (
    'NAME' => 'Бухгалтерия, финансы ',
    'CODE' => '1accounting_finance',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '47',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  10 => 
  array (
    'NAME' => 'Издательства, Полиграфия, СМИ',
    'CODE' => '1publishers_printing_media',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '48',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  11 => 
  array (
    'NAME' => 'Информационные технологии',
    'CODE' => '1information_technology',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '49',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  12 => 
  array (
    'NAME' => 'Кадровые службы',
    'CODE' => '1staffing_services',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '50',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  13 => 
  array (
    'NAME' => 'Маркетинг, Реклама, PR',
    'CODE' => '1marketing_advertising_pr',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '51',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  14 => 
  array (
    'NAME' => 'Медицина, Фармация',
    'CODE' => '1medicine_and_pharmacy',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '52',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  15 => 
  array (
    'NAME' => 'Недвижимость',
    'CODE' => '1real_estate',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '53',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  16 => 
  array (
    'NAME' => 'Образование и наука',
    'CODE' => '1education_and_science',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '54',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  17 => 
  array (
    'NAME' => 'Охрана, безопасность',
    'CODE' => '1safety_security',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '55',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  18 => 
  array (
    'NAME' => 'Персонал офиса ',
    'CODE' => '1office_staff',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '56',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  19 => 
  array (
    'NAME' => 'Производство, Промышленность',
    'CODE' => '1manufacturing_industry',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '57',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  20 => 
  array (
    'NAME' => 'Работа за рубежом',
    'CODE' => '1working_abroad',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '63',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  21 => 
  array (
    'NAME' => 'Работа на дому',
    'CODE' => '1work_at_home',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '58',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  22 => 
  array (
    'NAME' => 'Строительство, Архитектура',
    'CODE' => '1construction_architecture',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '60',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  23 => 
  array (
    'NAME' => 'Сфера обслуживания',
    'CODE' => '1service_industry',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '62',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  24 => 
  array (
    'NAME' => 'Торговля ',
    'CODE' => '1trade',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '64',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  25 => 
  array (
    'NAME' => 'Транспорт, Автобизнес',
    'CODE' => '1transport_autobusiness',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '59',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  26 => 
  array (
    'NAME' => 'Юриспруденция',
    'CODE' => '1jurisprudence',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '61',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  27 => 
  array (
    'NAME' => 'Бухгалтерия, финансы ',
    'CODE' => '1accounting_finance',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '65',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  28 => 
  array (
    'NAME' => 'Издательства, Полиграфия, СМИ',
    'CODE' => '1publishers_printing_media',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '66',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  29 => 
  array (
    'NAME' => 'Информационные технологии',
    'CODE' => '1information_technology',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '67',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  30 => 
  array (
    'NAME' => 'Кадровые службы',
    'CODE' => '1staffing_services',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '68',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  31 => 
  array (
    'NAME' => 'Маркетинг, Реклама, PR',
    'CODE' => '1marketing_advertising_pr',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '69',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  32 => 
  array (
    'NAME' => 'Медицина, Фармация',
    'CODE' => '1medicine_and_pharmacy',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '70',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  33 => 
  array (
    'NAME' => 'Недвижимость',
    'CODE' => '1real_estate',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '71',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  34 => 
  array (
    'NAME' => 'Образование и наука',
    'CODE' => '1education_and_science',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '72',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  35 => 
  array (
    'NAME' => 'Охрана, безопасность',
    'CODE' => '1safety_security',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '73',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  36 => 
  array (
    'NAME' => 'Персонал офиса ',
    'CODE' => '1office_staff',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '74',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  37 => 
  array (
    'NAME' => 'Производство, Промышленность',
    'CODE' => '1manufacturing_industry',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '75',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  38 => 
  array (
    'NAME' => 'Работа за рубежом',
    'CODE' => '1working_abroad',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '76',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  39 => 
  array (
    'NAME' => 'Работа на дому',
    'CODE' => '1work_at_home',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '77',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  40 => 
  array (
    'NAME' => 'Строительство, Архитектура',
    'CODE' => '1construction_architecture',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '78',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  41 => 
  array (
    'NAME' => 'Сфера обслуживания',
    'CODE' => '1service_industry',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '79',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  42 => 
  array (
    'NAME' => 'Торговля ',
    'CODE' => '1trade',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '80',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  43 => 
  array (
    'NAME' => 'Транспорт, Автобизнес',
    'CODE' => '1transport_autobusiness',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '81',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  44 => 
  array (
    'NAME' => 'Юриспруденция',
    'CODE' => '1jurisprudence',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '82',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
  ),
  45 => 
  array (
    'NAME' => 'Бизнес и Финансы',
    'CODE' => '1business',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '158',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
    'CHILDS' => 
    array (
      0 => 
      array (
        'NAME' => 'Банки',
        'CODE' => '1banks',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '159',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      1 => 
      array (
        'NAME' => 'Маркетинг',
        'CODE' => '1marketing',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '160',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      2 => 
      array (
        'NAME' => 'Недвижимость',
        'CODE' => '1real_estate',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '166',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      3 => 
      array (
        'NAME' => 'Страхование',
        'CODE' => '1insurance',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '167',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
    ),
  ),
  46 => 
  array (
    'NAME' => 'Дом и Семья',
    'CODE' => '1home',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '148',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
    'CHILDS' => 
    array (
      0 => 
      array (
        'NAME' => 'Дети',
        'CODE' => '1kids',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '170',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      1 => 
      array (
        'NAME' => 'Ремонт',
        'CODE' => '1repair',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '168',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      2 => 
      array (
        'NAME' => 'Товары для дома',
        'CODE' => '1homeware',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '169',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
    ),
  ),
  47 => 
  array (
    'NAME' => 'Наука и образование',
    'CODE' => '1education',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '154',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
    'CHILDS' => 
    array (
      0 => 
      array (
        'NAME' => 'ВУЗы',
        'CODE' => '1universities',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '171',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      1 => 
      array (
        'NAME' => 'Наука',
        'CODE' => '1science',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '172',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      2 => 
      array (
        'NAME' => 'Школы',
        'CODE' => '1schools',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '174',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      3 => 
      array (
        'NAME' => 'Экология',
        'CODE' => '1ecology',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '173',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
    ),
  ),
  48 => 
  array (
    'NAME' => 'Общество и Политика',
    'CODE' => '1society_and_politics',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '156',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
    'CHILDS' => 
    array (
      0 => 
      array (
        'NAME' => 'Общественные организации',
        'CODE' => '1public_organizations',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '178',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      1 => 
      array (
        'NAME' => 'Партии',
        'CODE' => '1party',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '177',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      2 => 
      array (
        'NAME' => 'Религия',
        'CODE' => '1religion',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '176',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
    ),
  ),
  49 => 
  array (
    'NAME' => 'Отдых и развлечения',
    'CODE' => '1recreation',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '155',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
    'CHILDS' => 
    array (
      0 => 
      array (
        'NAME' => 'Знакомства',
        'CODE' => '1dating',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '180',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      1 => 
      array (
        'NAME' => 'Клубы',
        'CODE' => '1clubs',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '182',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      2 => 
      array (
        'NAME' => 'Рыбалка',
        'CODE' => '1fishing',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '181',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
    ),
  ),
  50 => 
  array (
    'NAME' => 'СМИ',
    'CODE' => '1media',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '152',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
    'CHILDS' => 
    array (
      0 => 
      array (
        'NAME' => 'Газеты',
        'CODE' => '1newspapers',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '187',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      1 => 
      array (
        'NAME' => 'Радио',
        'CODE' => '1radio',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '188',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      2 => 
      array (
        'NAME' => 'Телевидение',
        'CODE' => '1television',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '189',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
    ),
  ),
  51 => 
  array (
    'NAME' => 'Товары и Услуги',
    'CODE' => '1products',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '150',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
    'CHILDS' => 
    array (
      0 => 
      array (
        'NAME' => 'Аптеки',
        'CODE' => '1pharmacies',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '195',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      1 => 
      array (
        'NAME' => 'Гостиницы',
        'CODE' => '1hotels',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '194',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      2 => 
      array (
        'NAME' => 'Мебель',
        'CODE' => '1furniture',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '197',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
      3 => 
      array (
        'NAME' => 'Одежда',
        'CODE' => '1clothing',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '196',
        'DESCRIPTION' => NULL,
        'DESCRIPTION_TYPE' => 'text',
      ),
    ),
  ),
  52 => 
  array (
    'NAME' => 'Олег Фахрутдин',
    'CODE' => '1user_1',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '705',
    'DESCRIPTION' => NULL,
    'DESCRIPTION_TYPE' => 'text',
    'CHILDS' => 
    array (
      0 => 
      array (
        'NAME' => 'Корпоративный выезд на косу',
        'CODE' => '1NULL',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '751',
        'DESCRIPTION' => 'День рождения компании на косе.',
        'DESCRIPTION_TYPE' => 'html',
      ),
    ),
  ),
)        );
    }

    public function down()
    {
        //your code ...
    }
}
