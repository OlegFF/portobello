<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
global $APPLICATION;
$APPLICATION->ShowBodyScripts();
$APPLICATION->ShowHead();
?>
<div class="container">
    <div class="row">
        <div class="col"><br>
        <?$APPLICATION->IncludeComponent(
            "sprint:main.feedback",
            "bootstrap_v4",
            array(
                "USE_CAPTCHA" => "Y",
                "OK_TEXT" => "Спасибо, ваша заявка принята.",
                "EMAIL_TO" => "my@email.com",
                "REQUIRED_FIELDS" => array(
                    0 => "NAME",
                    1 => "EMAIL",
                    2 => "MESSAGE",
                ),
                "EVENT_MESSAGE_ID" => array(
                ),
                "COMPONENT_TEMPLATE" => "bootstrap_v4"
            ),
            false
        );?>
        </div>
    </div>
</div>
<?//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
//require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_after.php');?>
